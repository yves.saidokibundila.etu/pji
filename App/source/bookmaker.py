import requests
import json


def get_last_game(league_id=2790,number_match=10):
    """ 
    Cette fonction nous renvoie les number_match derniers match de la ligue league_id 
    Cette fonction nous sert d'aider pour la conception, les saisons etant finies, plus de match futur pour predire
    """
    try :
        url = f"https://api-football-v1.p.rapidapi.com/v2/fixtures/league/{league_id}/last/{number_match}"
        #url = "https://api-football-v1.p.rapidapi.com/v2/fixtures/league/3/next/10"

        querystring = {"timezone":"Europe/London"}

        headers = {
            'x-rapidapi-key': "MY_KEY",
            'x-rapidapi-host': "api-football-v1.p.rapidapi.com"
            }

        response = requests.request("GET", url, headers=headers, params=querystring)
    except requests.exceptions.RequestExecption as e:
        return None

    fixtures=[]

    if(response.status_code==200):
        #decode en json
        response=response.json()
        fixtures=response['api']['fixtures']
        #recuperation des attributs home team et away team
        fixtures=[{'homeTeam':f['homeTeam'],
                'awayTeam':f['awayTeam'],'event_date': f["event_date"]} 
                for f in fixtures]

    return fixtures

def get_next_game(league_id=2790,number_match=10):
    """ Cette fonction nous renvoie les number_match prochains match de la ligue league_id """
    try :
        url = f"https://api-football-v1.p.rapidapi.com/v2/fixtures/league/{league_id}/next/{number_match}"
        #url = "https://api-football-v1.p.rapidapi.com/v2/fixtures/league/3/next/10"

        querystring = {"timezone":"Europe/London"}

        headers = {
            'x-rapidapi-key': "MY_KEY",
            'x-rapidapi-host': "api-football-v1.p.rapidapi.com"
            }

        response = requests.request("GET", url, headers=headers, params=querystring)
    except requests.exceptions.RequestExecption as e:
        return None

    return response.json()

#id league Premier league 2790
#id league ligue 1 2664
#id league liga 2833

#On peut acceder aux ligue selon les saison
