from pymongo import MongoClient
import pandas as pd
import json
from source import app
import os

db=None

def intitalisation():
    """ Initlialise la base de donnee """

    global db
    database_name='football_database'
    mongodb_password='password'
    DB_url=f"mongodb+srv://saido:{mongodb_password}@cluster0.vyzwr.mongodb.net/{database_name}?retryWrites=true&w=majority"

    client_database=MongoClient(DB_url)
    db=client_database.football_database.team

    return db 

def fill_database(filename, header=0):
    """ 
    Remplissage de la base de donnee a partir des donnees contenue dans un fichier json.
    Ce fichier contient les donnees des chaque equipe 
    """
    
    res=os.getcwd()
    
    df = pd.read_csv(res+filename, header=header)
    df.drop(columns=['index'],inplace=True)
    df['league']=filename[6:-4]

    result = df.to_json(orient="records")
    parsed = json.loads(result)
    
    db.insert_many(parsed)
    
    return True


def get_league():
    """ Renvoie les ligue disponible dans notre base de donnee """
    return [ doc for doc in db.aggregate( [ { "$group" : { "_id" : "$league" } } ] )]


def get_teams(league):
    """ Renovoie les donnees de toutes les equipes pour la ligue passee en parametre """
    res=[]
    for doc in db.find({"league":league}):
        doc.pop('_id')
        res.append(doc)
    return res


if __name__=='__main__':
    intitalisation()
    fill_database()
