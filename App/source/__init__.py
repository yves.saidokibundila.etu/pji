from flask import Flask,request,jsonify,Response
from flask_restful import reqparse, abort, Api, Resource 
from flask import render_template,url_for,flash,redirect
from flask.views import MethodView
from flask_mongoengine import MongoEngine
import pickle 
from source import bookmaker
import pandas as pd 
import numpy as np
import os

app = Flask(__name__)
api = Api(app)

#imporation base de donnee
from source.database import intitalisation,fill_database,get_league,get_teams

#initilisation et remplisation de la base de donnee
db=intitalisation()
db.drop()
fill_database('/data/Premier League.csv')
fill_database('/data/Ligue 1.csv')
fill_database('/data/Primera Division.csv')

#model
res=os.getcwd()
model=pickle.load(open(res+'/source/model.pkl','rb'))

#Route inexistant
def abort_if_todo_doesnt_exist(todo_id):
    """ renvoie l'erreur pour une route inexistant """

    res=[e['_id'] for e in get_league()]
    
    if todo_id not in res:
        abort(404, message="ligue_id {} n'exist !".format(todo_id))


#On pouvait faire un choix en effectuant les requetes pour chaque equipe, 
#mais nous avons tout telecharger pour ne pas vider notre quota

def preprocess_match(match,team_data):
    """ permet de recuperer les donnees pour une rencontre, et prediction avec le model entraine """

    #recuperation des equipes pour la rencontre
    hometeam=match['homeTeam']['team_name']
    hometeam_logo=match['homeTeam']['logo']
    awayteam=match['awayTeam']['team_name']
    awayteam_logo=match['awayTeam']['logo']

    hometeam_stat=None
    awayteam_stat=None
    
    #recuperation stat
    for t in team_data:
        if(t["Team"]==hometeam):
            hometeam_stat=t 
        elif (t["Team"]==awayteam):
            awayteam_stat=t

    #traitement pour faire la prediction   
    feature_home=['HomeTeam','HTGS','HTGC','HTP','HTGD']
    feature_away=['AwayTeam','ATGS','ATGC','ATP','ATGD']
    new_feature=['Team','GS','GC','TP','GD']

    if (hometeam_stat==None) or (awayteam_stat==None):
        return [] 

    stat = {'HTGS':hometeam_stat['GS'],
        'ATGS':awayteam_stat['GS'],
        'HTGC':hometeam_stat['GC'],
        'ATGC':awayteam_stat['GC'],
        'HTP':hometeam_stat['TP'],
        'ATP':awayteam_stat['TP'],
        'HTGD':hometeam_stat['GD'],
        'ATGD': awayteam_stat['GD'] 
        }

    #prediction    
    stat=np.array(list(stat.values())).reshape((1,-1))
    pred=model.predict_proba(stat)
    
    pred={
        'home_prob':pred[0][0],
        'draw_prob':pred[0][1],
        'away_prob':pred[0][2]
    }

    data= {
        'HomeTeam':hometeam,
        'AwayTeam':awayteam,
        'hometeam_logo':hometeam_logo,
        'awayteam_logo':awayteam_logo,
        'prediction':pred,
        'date':match['event_date'][:10]
    }
    
    #print('DONE')

    return data


#les chemins pour l'application web
#app.route('/',methods=['POST','GET'])
#@api.representation('text/html')
class Home(MethodView):
    @api.representation('text/html')
    def res(self,data, code, headers=None):
        resp=Response(data,code)
        resp.headers.extend(headers or {})
        return resp
    def get(self):
    #return render_template('home.html')
        return self.res(render_template('home.html'),200)


class Pred_view(MethodView):
    @api.representation('text/html')
    def res(self,data, code, headers=None):
        resp=Response(data,code)
        resp.headers.extend(headers or {})
        return resp
    def get(self):
    #return render_template('home.html')
        return self.res(render_template('pred.html'),200)


class item_view(MethodView):
    @api.representation('text/html')
    def res(self,data, code, headers=None):
        resp=Response(data,code)
        resp.headers.extend(headers or {})
        return resp
    def post(self):
        
        league_id=request.form.get('league')
    #return render_template('home.html')
        print('===================')
        print(league_id)
        res=self.helper(league_id)
        print(res)
        return self.res(render_template('item.html', data=res,league=league_id),200)


    def helper(self,league_id):
        abort_if_todo_doesnt_exist(league_id)

        tb_league={
            'Premier League':2790,
            'Ligue 1':2664,
            'Primera Division':2833
        }
        #recuperer les matchs depuis l'api bookmaker
        match_days=bookmaker.get_last_game(tb_league[league_id])
        #print(match_days)
        #recuperer les informations de chaque equipe de la ligue depuis la base de donnee 
        teams_data=get_teams(league_id)
        #faire le traitement de ce donnees, preprocesssing, la prediction avec le modele
        all_data=[]
        for match in match_days:
            data=preprocess_match(match,teams_data)
            if(data!=[]):
                all_data.append(data)       
        
        #renvoyer les predictions de chaque match 
        return all_data



class Infos(Resource):
    """ Information renvoye pour avoir les informations quand a l'utilisation de l'api """
    def get(self):
        return jsonify({'Infos':'Information concernant API'})
    def post(self):
        return {}

class League(Resource):
    """ renvoie les ligue  disponible dans notre api """
    def get(self):
        res={
            "league": get_league()
        }
        return jsonify(res) 
    def post(self):
        return {}

class Prediction(Resource):
    """ renvoie la prediction des 10 prochaines rencontres d'une ligue qu'on precise """
    def get(self,league_id):
        
        abort_if_todo_doesnt_exist(league_id)

        tb_league={
            'Premier League':2790,
            'Ligue 1':2664,
            'Primera Division':2833
        }
        #recuperer les matchs depuis l'api bookmaker
        match_days=bookmaker.get_last_game(tb_league[league_id])
        #print(match_days)
        #recuperer les informations de chaque equipe de la ligue depuis la base de donnee 
        teams_data=get_teams(league_id)
        #faire le traitement de ce donnees, preprocesssing, la prediction avec le modele
        all_data=[]
        for match in match_days:
            data=preprocess_match(match,teams_data)
            if(data!=[]):
                all_data.append(data)       
        
        #renvoyer les predictions de chaque match 
        send={
            "data":all_data
        }

        return jsonify(send)


    #pour obtenir les prediction de la saison
    def post(self,league_id):
        abort_if_todo_doesnt_exist(league_id)
        return {'data':'POST'}

api.add_resource(Prediction,'/prediction/<string:league_id>')
api.add_resource(Infos,'/infos')
api.add_resource(League,'/league')
api.add_resource(Home,'/')
api.add_resource(Pred_view,'/pred')
api.add_resource(item_view,'/item',methods=['POST','GET'])

if __name__=='__main__':
    app.run(debug=True)
