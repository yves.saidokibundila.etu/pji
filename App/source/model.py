import pandas as pd
import numpy as np
from sklearn.metrics import accuracy_score
from sklearn.linear_model import LogisticRegression
import pickle

#chargement de donnees
chemin='../data_train/'
data_train=pd.read_csv(chemin+'Dataset_train.csv')
data_test=pd.read_csv(chemin+'Dataset_test.csv')

print('loading data Done ...')

#preprocessing
data_train=data_train.drop(columns=['0']).dropna(axis=0)
data_test=data_test.drop(columns=['0']).dropna(axis=0)

feature=['HTGS','ATGS','HTGC','ATGC','HTP','ATP','HTGD','ATGD','FTR']
#feature=['HTGS','ATGS','HTGC','ATGC','HTP','ATP','HTGD','ATGD','DiffPts','DiffFormPts','FTR']


data_train=data_train[feature]
data_test=data_test[feature]

y_train=data_train['FTR']
X_train=data_train.drop(columns=['FTR'])

y_test=data_test['FTR']
X_test=data_test.drop(columns=['FTR'])


#training model
lgr=LogisticRegression(max_iter=500)
lgr.fit(X_train,y_train)
print('training model Done ...')


y_pred = lgr.predict(X_test)
accuracy = accuracy_score(y_test, y_pred)
#print("Accuracy: %.2f%%" % ( accuracy*100.0 ))


#saving model
pickle.dump(lgr,
open('model.pkl','wb'))
print('saving model Done ...')
