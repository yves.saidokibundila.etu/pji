from speechbrain.pretrained import EncoderDecoderASR

#asr_model = EncoderDecoderASR.from_hparams(source="/home/ubuntu/result_char/CRDNN_BPE_960h_LM/2602/save/CKPT+2021-06-24+22-12-44+00", savedir="tmp")

asr_model = EncoderDecoderASR.from_hparams(source="tmp_files/",
    hparams_file='./hyperparams.yaml' ,savedir='tmp')

audio_file = '/home/ubuntu/ml/egs/personnalized_Name/data/mp3/b8ef661513f1c5e2cda9a0b19aeb560d92a2b086.mp3'
#"ALAIN FICHELLE"
audio_file2='/home/ubuntu/Stage/ml/egs/personnalized_Name/data/mp3/8134de83336ee4bfed541568f729cc3740221cf9.mp3'
#"christophe DALLA TORRE"

res=asr_model.transcribe_file(audio_file)
res2=asr_model.transcribe_file(audio_file2)

print(res)
print(res2)

